/*
Test_Sensors - A test program to see if the sensors are working
   
   Sam Walder - University of Bristol - 2015 - sam.walder@bristol.ac.uk

   Change Log:
       * 2016/04 - Created
 */

// Set up some variables
#define sensorPin A0    // Set the analog in that the sensor is on

// Setup function
void setup()
{
  // Start serial port at 9600 bps:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  
  //pinMode(sensorPin, INPUT);   // Analog sensor
  Serial.println("Online");       // Send a hello message
}

// Main loop
void loop()
{
  // Send sensor values:
  Serial.println(readSensor());
}

int readSensor() {
  int readBlack = 175;
  int readWhite = 5;
  int reading;
  // First map from analog read to standard 0-255
  reading = map(analogRead(sensorPin), 0, 1024, 0, 254);
  // Then map from this to -10 to +10 (given the set black and white read values)
  reading = map(reading, readWhite, readBlack, -100, 100);
  return reading;
}
