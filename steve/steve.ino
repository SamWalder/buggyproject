
/*****************************************************************************
 *   steve.c: Main file for a line following buggy
 *
 *   Copyright(C) 2016
 *      -> Jeremy Dalton (jd0185@my.bristol.ac.uk)
 *      -> Sam Walder (sam.walder@bristol.ac.uk)
 *   All rights reserved.
 *
 ******************************************************************************/
 
/******************************************************************************
 * Includes
 *****************************************************************************/
#include <PID_v1.h>     // Install this through the library manager (PID)
#include <TimerOne.h>   // Install this through the library manager (TimerOne)

/******************************************************************************
 * Defines
 *****************************************************************************/
// Set up some motor pins
#define mLDir 4
#define mLSpd 5
#define mRDir 7
#define mRSpd 6

// Setup the sensor pin
#define sensorPin A0

// One for the LED
#define led 13

/******************************************************************************
 * Global variables
 *****************************************************************************/
volatile unsigned adcVal = 0; // use volatile for shared variables

// Set the sample time that will be used in the controll loop
int sampleTime_ms = 10;

// PID controller things:
// Define Variables we'll be connecting the PID control loop to
double Setpoint = 0, Input = 0, Output = 0;
double Kp = 0.5, Ki = 0.5, Kd = 0.01;
// Specify the links and initial PID tuning parameters
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);

// Set up some variables for the zero crossing detector
int outputOld = 0;
int outputNew = 0;
int counter = 0;
int countDisable = 0;
int readingNew;

// Variable to invert the input if we think that things have gone backwards
int invert = 1;

/******************************************************************************
 * SETUP FUNCTION
 *****************************************************************************/
// Setup function
void setup (void)
{
  // Set the led pin
  pinMode(led, OUTPUT);

  // Start serial port at 9600 bps:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  // Setup the PID controller
  myPID.SetMode(AUTOMATIC); // MANUAL = Off, AUTOMATIC = on
  myPID.SetSampleTime(sampleTime_ms);  // 10ms
  myPID.SetControllerDirection(REVERSE );
  myPID.SetOutputLimits(-100, 100);

  // Setup the timer interrupt
  Timer1.initialize(sampleTime_ms*1000); // This is in uS
  Timer1.attachInterrupt(T1_ISR);

  // Initilise the motors
  intMotors();
}

/******************************************************************************
 * MAIN FUNCTION
 *****************************************************************************/
// Main loop
void loop (void)
{


}


/******************************************************************************
 * ISR Handeler function
 *****************************************************************************/
void T1_ISR (void)
{
  //myPID.Compute();
  readingNew = readSensor();
  Input = invert*readingNew;
  //Serial.println(Input);
  myPID.Compute();
  //Serial.println(Output);
  setMotors(20, round(Output));

  // Stuff to check the time between zero crossings
  outputOld = outputNew;
  outputNew = readingNew;
  if (sgn(outputNew) != sgn(outputOld)){
    // Zero crossing was detected

    // Cap the counter time
    if (counter>100){
      counter = 100;
    }

    // Print what the counter got to and then reset it
    //Serial.println(counter);
    counter = 0;

    // Undisable the counter
    countDisable = 0;
  }
  // Increment the counter
  if (countDisable != 1){
    counter += 1;
  }

  // If the counter gets to more than our threshold then invert the input
  if (counter > 20) {
    invert = invert * -1;
    countDisable = 1;
    counter = 0;
  }

  Serial.println(invert);
}

/******************************************************************************
 * setMotors function
 *    Sets the speeds of the motors
 *****************************************************************************/
void setMotors(int commonMode, int diffMode) {
  // This function will set the motors going
  // Inputs should be specified in the range -100<x<100
    // commonMode = -100  -  Full backwards
    // commonMode = 0     -  Stop
    // commonMode = 100   -  Full forwards
    // diffMode = -100    -  Turn left on the spot
    // diffMode = 0       -  No turn
    // diffMode = 100     -  Turn right on the spot
  
  // Figure out the speed for each motor
  int mLSpeed = commonMode + diffMode;
  int mRSpeed = commonMode - diffMode;

  
  // Echo this back
  /*
  Serial.print("mLSpeed = ");
  Serial.print(mLSpeed);
  Serial.print("  mRSpeed = ");
  Serial.println(mRSpeed);
  */
  
  
  // Figure out and set the direction of each motor
  if (mLSpeed > 0) {
    digitalWrite(mLDir, LOW);
  } else {
    digitalWrite(mLDir, HIGH);
  }
  if (mRSpeed > 0) {
    digitalWrite(mRDir, LOW);
  } else {
    digitalWrite(mRDir, HIGH);
  }
  
  // Set the speed of each motor
  analogWrite(mLSpd, map(abs(mLSpeed), 0, 100, 0, 255));
  analogWrite(mRSpd, map(abs(mRSpeed), 0, 100, 0, 255));  
}

/******************************************************************************
 * intMotors function
 *    Initilises the motor pins and speeds
 *****************************************************************************/
void intMotors() {
  // Set the pin modes
  pinMode(mLDir, OUTPUT);
  pinMode(mRDir, OUTPUT);
  pinMode(mLSpd, OUTPUT);
  pinMode(mRSpd, OUTPUT);
  
  // Set the motors to off
  setMotors(0, 0);
}

/******************************************************************************
 * readSensor function
 *    Reads the infrared reflectance sensor
 *****************************************************************************/
int readSensor() {
  int readBlack = 180;
  int readWhite = 5;
  int reading;
  // First map from analog read to standard 0-255
  reading = map(analogRead(sensorPin), 0, 1024, 0, 254);
  // Then map from this to -10 to +10 (given the set black and white read values)
  reading = map(reading, readWhite, readBlack, -100, 100);
  // Apply saturation to this
  if (reading>100) {
    reading = 100;
  }
  if (reading < -100) {
    reading = -100;
  }
  // Return
  return reading;
}

/******************************************************************************
 * sgn function
 *    Returns the sign of the input
 *****************************************************************************/
 int sgn(int input) {
  return input/abs(input);
 }

