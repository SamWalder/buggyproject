/*
Test_Motors - A test program to see if the motors are working

To use this, open the serial monitor. Type 8 characters in, the first 4 will be interprted as the commen mode speed, the last 4 as the differential mode speed
   
   Sam Walder - University of Bristol - 2015 - sam.walder@bristol.ac.uk

   Change Log:
       * 2016/04 - Created
 */

// To incorporate the motors you need 4 things.
//    - The pin definitions (below)
//    - The intMotors() function
//    - The setMotors() function
//    - An understanding of what you are doing
// Set up some motor pins
#define mLDir 4
#define mLSpd 5
#define mRDir 7
#define mRSpd 6

// Setup function
void setup()
{
  // Start serial port at 9600 bps:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  
  //pinMode(sensorPin, INPUT);   // Analog sensor
  Serial.println("Online");       // Send a hello message
  
  // Initilise the motors
  intMotors();
  
}

// Main loop
void loop()
{ 
  // Define some variables
  int i;
  String incoming_command;
  int vComm;
  int vDiff;

  // Wait for some serial data to turn up
  while(Serial.available() < 8){
    // Do nothing
  }
  for (i=1; i<=8; i++){
    incoming_command += (char)Serial.read();
  }

  // Echo this back
  Serial.println(incoming_command);
  
  // Decipher the two number message
  vComm = string2int(incoming_command.substring(0, 4));
  vDiff = string2int(incoming_command.substring(4, 8));
  
  // Echo this back
  Serial.print("vComm = ");
  Serial.println(vComm);
  Serial.print("vDiff = ");
  Serial.println(vDiff);
    
  setMotors(vComm, vDiff);
}

void setMotors(int commonMode, int diffMode) {
  // This function will set the motors going
  // Inputs should be specified in the range -100<x<100
  // commonMode = -100  -  Full backwards
  // commonMode = 0     -  Stop
  // commonMode = 100   -  Full
  // diffMode = -100    -  Turn left on the spot
  // diffMode = 0       -  No turn
  // diffMode = 100     -  Turn right on the spot
  
  // Figure out the speed for each motor
  int mLSpeed = commonMode + diffMode;
  int mRSpeed = commonMode - diffMode;
  
  // Echo this back
  Serial.print("mLSpeed = ");
  Serial.println(mLSpeed);
  Serial.print("mRSpeed = ");
  Serial.println(mRSpeed);
  
  // Figure out and set the direction of each motor
  if (mLSpeed > 0) {
    digitalWrite(mLDir, LOW);
  } else {
    digitalWrite(mLDir, HIGH);
  }
  if (mRSpeed > 0) {
    digitalWrite(mRDir, LOW);
  } else {
    digitalWrite(mRDir, HIGH);
  }
  
  // Set the speed of each motor
  analogWrite(mLSpd, mLSpeed);
  analogWrite(mRSpd, mRSpeed);  
}

void intMotors() {
  // Set the pin modes
  pinMode(mLDir, OUTPUT);
  pinMode(mRDir, OUTPUT);
  pinMode(mLSpd, OUTPUT);
  pinMode(mRSpd, OUTPUT);
  
  // Set the motors to off
  setMotors(0, 0);
}

int string2int(String inString){
  // Convert a string to an integer
  return inString.toInt();
}
